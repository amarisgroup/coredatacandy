# CoreDataCandy

All notable changes to this project will be documented in this file. `CoreDataCandy` adheres to [Semantic Versioning](http://semver.org).

---
## [0.1.0](https://gitlab.com/amarisgroup/coredatacandy/tree/0.1.0) (04/11/2020)

Initial release.